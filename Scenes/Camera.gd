extends Camera2D

export (int) var speed = 200

export(NodePath) var label 

var MIN_ZOOM = 0.1 
var MAX_ZOOM = 1.2

var OFFSET = Vector2(200, 0)

var velocity = Vector2()

var zoom_value = 1 setget zoom_value_set

func zoom_value_set(value): 
	zoom = Vector2(value, value)
	zoom_value = value
	offset = OFFSET * zoom


func get_keyboard_input():
	# If a UI element has focus, disable keyboard movement of the image
	var current_focus_control = $Control.get_focus_owner()
	if current_focus_control:
		return

	if Input.is_action_pressed("right"):
		velocity.x += 1
	if Input.is_action_pressed("left"):
		velocity.x -= 1
	if Input.is_action_pressed("down"):
		velocity.y += 1
	if Input.is_action_pressed("up"):
		velocity.y -= 1
	velocity = velocity.normalized()
	
func _input(event):
	var mouse_pos = get_local_mouse_position()
	if event is InputEventMouseMotion:
		if Input.is_action_pressed("secondary_click"):
			move_image(-event.relative * zoom_value)
	if event.is_action_pressed("zoom_in"):
		zoom_value = zoom_value - 0.1
		translate(0.1 * mouse_pos)
	if event.is_action_pressed("zoom_out"):
		zoom_value = zoom_value + 0.1
		translate(-0.1 * mouse_pos)
	# move camera so it the mouse pointer points at the same spot
	# get_node(label).text = String(mouse_pos)
	zoom_value_set( clamp(zoom_value, MIN_ZOOM, MAX_ZOOM) )

func move_image(vector: Vector2):
	translate(vector)
	# clamp the camera movement, so that the image is never out of view
	position = Vector2(
		clamp(position.x, 0, 1024),
		clamp(position.y, 0, 1024)
	)
	pass

func _process(delta):
	velocity = Vector2()
	get_keyboard_input()

	move_image(velocity * speed * delta * zoom_value)
	
func _on_reset_position():
	position = Vector2(512, 512)
	zoom_value_set(1)

