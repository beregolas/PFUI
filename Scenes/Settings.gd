extends Node

signal color_changed

var selected_color = Color.royalblue setget set_selected_color

func set_selected_color(value):
	selected_color = value
	emit_signal("color_changed", selected_color)
