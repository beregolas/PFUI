extends Control

# simply take focus away from the GUI if anything else is clicked
func _gui_input(event):
	if event is InputEventMouseButton:
		var current_focus_control = get_focus_owner()
		if current_focus_control:
			current_focus_control.release_focus()
