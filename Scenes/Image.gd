extends Sprite

export(NodePath) var label 

func _input(event):
	if event is InputEventMouseMotion:
		var pos = get_local_mouse_position()
		# get_node(label).text = String(pos)
	
	pass

func _ready():
	$ImageRequest.connect("request_completed", self, "_on_receive_image")
	reload()
	

func reload():
	self.texture = null
	var error = $ImageRequest.request("https://gebirge.uber.space/place.png", [], false, HTTPClient.METHOD_GET)
	if error != OK:
		push_error("error connecting to place!")

func _on_receive_image(result, response_code, headers, body):
	var image = Image.new()
	var error = image.load_png_from_buffer(body)
	if error != OK:
		push_error("Image loading not sucessful!")
	
	var newTexture = ImageTexture.new()
	newTexture.create_from_image(image, 1)
	
	self.texture = newTexture
