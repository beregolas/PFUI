extends Node2D

var focus = Vector2(512, 512)

var color = Color(1, 0, 0, 0.5)

func _update_color(new_color): 
	color = new_color
	update()


func _ready():
	$"/root/Settings".connect("color_changed", self, "_update_color")
	pass


func _draw():
	var line_color = Color(1-color.r, 1-color.g, 1-color.b, 0.5)
	var box_color = Color(color.r, color.g, color.b, 0.3)
	
	# Vertical
	draw_line(Vector2(0, focus.y), Vector2(1024, focus.y), line_color)
	draw_line(Vector2(0, focus.y+1), Vector2(1024, focus.y+1), line_color)
	# Horizontal
	draw_line(Vector2(focus.x, 0), Vector2(focus.x, 1024), line_color)
	draw_line(Vector2(focus.x+1, 0), Vector2(focus.x+1, 1024), line_color)
	
	# Box
	draw_rect(Rect2(focus, Vector2(1,1)), box_color)
	
	pass
